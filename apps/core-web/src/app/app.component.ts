import { Component } from '@angular/core';

@Component({
  selector: 'ds-freud-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'core-web';
}
